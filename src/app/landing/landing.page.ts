import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
  }

  async login(type) {
    // if(type==1){
    //   const loginModal = await this.modalController.create({
    //     component: LoginPage,
    //   });
    //   return await loginModal.present();
    // }else if (type == 2){
    //   const loginModal = await this.modalController.create({
    //     component: Login2Page,
    //   });
    //   return await loginModal.present();
    // }
    if(type==1){
      this.navCtrl.navigateRoot('/login2');
    } else {
      this.navCtrl.navigateRoot('/login');
    }
    
  }

}
