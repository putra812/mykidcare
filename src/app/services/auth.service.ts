import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { EnvService } from './env.service';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token:any;
  constructor(
    private http: HttpClient,
    private storage: Storage,
    private env: EnvService,
  ) { }
  postData(data, path) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      console.log(data);
      this.http.post(this.env.API_URL + path, JSON.stringify(data))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    }); 

  } 
  login(nokp: String, password: String) {
    return this.http.post(this.env.API_URL + 'loginguru',
      {nokp: nokp, katalaluan: password}
    ).pipe(
      tap(token => {
        this.storage.set('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        this.token = token;
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  register(fName: String, lName: String, email: String, password: String) {
    return this.http.post(this.env.API_URL + 'auth/register',
      {fName: fName, lName: lName, email: email, password: password}
    )
  }
  logout() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    });
    return this.http.get(this.env.API_URL + 'auth/logout', { headers: headers })
    .pipe(
      tap(data => {
        this.storage.remove("token");
        this.isLoggedIn = false;
        delete this.token;
        return data;
      })
    )
  }
  user() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    });
    return this.http.get<User>(this.env.API_URL + 'auth/user', { headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }
  // getToken() {
  //   return this.storage.getItem('token').then(
  //     data => {
  //       this.token = data;
  //       if(this.token != null) {
  //         this.isLoggedIn=true;
  //       } else {
  //         this.isLoggedIn=false;
  //       }
  //     },
  //     error => {
  //       this.token = null;
  //       this.isLoggedIn=false;
  //     }
  //   );
  // }
  getUser() {
    return this.storage.get('userData').then(
      data => {
        this.user = data;
        if(this.user != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
      },
      error => {
        this.user = null;
        this.isLoggedIn=false;
      }
    );
  }
  getKelas(){
    return new Promise((resolve, reject) => {
    this.storage.get("userData").then(res=>{
      if(res){
        var obj:any = res;
        console.log(obj.idkelas);
        // return obj.idkelas
        resolve(obj.idkelas);
      }
    }, err => {
      console.log(err);
      reject(err);
    })
  })
  }
  getSenaraiMurid(){
    return new Promise((resolve, reject) => {
      this.storage.get("senaraiMurid").then(res=>{
        if(res){
          var obj:any = res;
          console.log(res);
          resolve(obj);
        } else {
          console.log("bbbbbbbbbbbbbbb")
          var id:any = "";
          var bar1 = new Promise((resolve, reject) => {
            this.getKelas().then(res=>{
              console.log(res);
              id=res;
              resolve();
            });
            
          })
          bar1.then(()=>{
            var data = {idkelas : id};
            this.postData(data,"senaraimurid").then(senarai=>{
              if(senarai){
                var list:any = senarai
                this.storage.set("senaraiMurid",list.senaraimurid);
                resolve(list);
              }
            }, error =>{
              reject(error);
            })
          })
          
        }
      }, err =>{
        reject(err);
      })
    })
  }
  getListSebab(){
    return new Promise((resolve,reject) =>{
      this.storage.get("listSebab").then(res=>{
        if(res){
          var obj:any = res;
          resolve(obj);
        } else {
          this.postData('','senaraisebabtidakhadir').then(res1=>{
            if(res1){
              var obj:any = res1;
              resolve(obj.senaraisebabtidakhadir);
            }
          }, error=>{
            reject(error);
          })
        }
      }, error=>{
        reject(error);
      })
    })
  }

  getCurrentDate(){
    // return new Promise((resolve,reject) =>{
      //return string
      var returnDate = "";
      //get datetime now
      var today = new Date();
      //split
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //because January is 0! 
      var yyyy = today.getFullYear();
      //Interpolation date
      returnDate += `${yyyy}-`;
      if (mm < 10) {
        returnDate += `0${mm}-`;
    } else {
        returnDate += `${mm}-`;
    }
      if (dd < 10) {
          returnDate += `0${dd}`;
      } else {
          returnDate += `${dd}`;
      }
      
      // resolve(returnDate);
      return returnDate;
    // })
  }
}