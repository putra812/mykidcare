import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SitterPage } from './sitter.page';

describe('SitterPage', () => {
  let component: SitterPage;
  let fixture: ComponentFixture<SitterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SitterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
