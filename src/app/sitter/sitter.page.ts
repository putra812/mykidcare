import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sitter',
  templateUrl: './sitter.page.html',
  styleUrls: ['./sitter.page.scss'],
})
export class SitterPage implements OnInit {
  sitter: { id: string; name: string; loc: string; img: string; age: string; desc: string; }[];

  constructor() { }

  ngOnInit() {
    this.sitter = [
      {
        id : "1",
        name : "Farah Idilia",
        loc : "Puchong",
        img : "sitter1.jpg",
        age: "24",
        desc : "Hi I am Aishah, a part time babysitter. I have experience working as a kindergarten teacher before, and babysitting my nieces and nephews a lot since high school."
      },
      {
        id : "2",
        name : "Tengku Iman",
        loc : "Seri Kembangan",
        img : "sitter2.jpg",
        age : "21",
        desc : "I'm always babysitting! It comes naturally to me! hdjdjdjdnsnksjskdkndndndmdndmdkdkdkdkkdkdkdkdkdkdjfkfkfkfkfkkdkdjdndndnfjjfjfjfjfkfkfkffkfkfkfkfkfkfkfjfkfkdkdkfkfkfkfkfkkfkfkfkfkfkfkfkkfkfkkkkkkkkkkkkkkkkkkkkkki dont know what else to say"
      },
      {
        id : "3",
        name : "Hanis",
        loc : "Cyberjaya",
        img : "sitter3.jpg",
        age : "23",
        desc : "Hai, Nama Saya hanis. Berumur 23 Tahun. Saya pelajar dari Kolej Islam Antarabangsa mengambil kursus sijil Pendidikan Awal Kanak-kanak."
      }
    ]
  }

}
