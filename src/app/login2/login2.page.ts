import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
// import { RegisterPage } from '../register/register.page';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login2',
  templateUrl: './login2.page.html',
  styleUrls: ['./login2.page.scss'],
})
export class Login2Page implements OnInit {
  
  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }

  // On Register button tap, dismiss login modal and open register modal
  // async registerModal() {
  //   this.dismissLogin();
  //   const registerModal = await this.modalController.create({
  //     component: RegisterPage
  //   });
  //   return await registerModal.present();
  // }

  login(form: NgForm) {
    this.navCtrl.navigateRoot('/tabs2');
    // this.dismissLogin();
    //     // this.navCtrl.navigateRoot('/teacher/home');
    // var data = {nokp: form.value.email, katalaluan: form.value.password, onesignal_id: localStorage.getItem("player_id")}
    // this.authService.postData(data, 'loginguru').then(
    //   data => {
    //     console.log(data);
        

    //     var obj:any = data;
    //     if(obj.kod == "0"){
    //       this.alertService.presentToast(obj.message);
    //       this.storage.set('userData',obj);

    //       var data2 = {nokp: obj.nokp, idsekolah: obj.idsekolah};
    //       this.authService.postData(data, 'infoguru').then(info =>{
    //         this.storage.set('infoguru',info);
    //       })
    //       // this.dismissLogin();
    //       this.navCtrl.navigateRoot('/teacher/home');
    //     } else {
    //       this.alertService.presentToast(obj.message);
    //       this.dismissLogin();
    //     }
    //   },
    //   error => {
    //     console.log(error);
    //   });
  }
}