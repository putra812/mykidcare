import { Component } from '@angular/core';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-tab5',
  templateUrl: 'tab5.page.html',
  styleUrls: ['tab5.page.scss']
})
export class Tab5Page {
  sitter: { id: string; name: string; loc: string; img: string; age: string; desc: string; }[];

  constructor(private alertSvc: AlertService) {}

  ionViewDidEnter(){
    this.alertSvc.presentToast("Welcome back");
    this.sitter = [
      {
        id : "1",
        name : "Izatti",
        loc : "Kuala Lumpur",
        img : "baby1.jpg",
        age: "34",
        desc : "Kami adalah Izatti dan Villon, ibu bapa yang sedang mencari seorang pengasuh untuk anak kami yang berumur 4 tahun. Kami memerlukan seseorang yang ada kereta, dan mempunyai kerusi bayi di dalamnya. Pengasuh hendaklah menjemput anak kami dari taska pukul 530pm dan membawa dia pulang dan memberi dia makan sampai kami balik pukul 7pm. Sila mesej kami jika awak berminat!"
      },
      {
        id : "2",
        name : "Alice",
        loc : "Seri Kembangan",
        img : "baby2.jpg",
        age : "31",
        desc : "Hai semua! Nama saya Alice dan saya seorang wartawan untuk sebuah syarikat di damansara. Saya adalah seorang ibu tunggal dan saya sedang mencari seorang pengasuh pada bahagian petang sehingga malam. Pengasuh yang berminat mestilah mengambil anak saya dari tadika pukul 5pm dan tunggu saya balik sehingga 10pm. Anak saya berumur 5 tahun dan dia suka melukis, makan buar pir dan tengok kartun. Sila mesej saya jika masa mengasuh ini sesuai dengan awak!"
      },
    ]
  }

}
